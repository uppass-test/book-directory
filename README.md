# book-directory

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Requirement

https://docs.google.com/document/d/1rIZQlDCloEhNgDr8kF3E4WqswzcLMjIIcBWXN5_7B8c/edit?usp=sharing
